import argparse
import train
import eval


def main():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers(dest='subcommand')
    parser_train = subparsers.add_parser('train')
    parser_train.add_argument('--train_data', type=str)
    parser_train.add_argument('--embeddings', type=str)
    parser_train.add_argument('--gpu', type=int, default=-1)
    parser_train.add_argument('--batchsize', type=int, default=128)
    parser_train.add_argument('--n_vocab', type=int, default=100000)
    parser_train.add_argument('--n_units', type=int, default=256)
    parser_train.add_argument('--epoch', type=int, default=10)
    parser_train.add_argument('--max_len', type=int, default=100)
    parser_eval = subparsers.add_parser('eval')
    parser_eval.add_argument('--test_data', type=str)
    parser_eval.add_argument('--embeddings', type=str)
    parser_eval.add_argument('--n_vocab', type=int, default=100000)
    parser_eval.add_argument('--n_units', type=int, default=256)
    parser_eval.add_argument('--max_len', type=int, default=100)
    parser_eval.add_argument('--model', type=str)

    args = parser.parse_args()
    if args.subcommand == 'train':
        train.train(args)
    elif args.subcommand == 'eval':
        eval.f_score(args)


if __name__ == '__main__':
    main()
