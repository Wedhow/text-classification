import collections
from gensim.models import KeyedVectors


def load_embeddings(embeddings, n_vocab, n_units):
    model = KeyedVectors.load_word2vec_format(embeddings, binary=True)
    dic = {w: i for i, w in enumerate(model.index2word[:n_vocab])}
    return model.vectors[:n_vocab, :n_units], dic


def create_dictionary(sentences, n):
    words = []
    dic = ['<UNK>']
    for sentence in sentences:
        words.extend(sentence.split())
    count_dict = collections.Counter(words)
    for word in count_dict.most_common(n-1):
        dic.append(word[0])
    return dic


def convert2id(sentences, word2id, max_len):
    data = []
    for sentence in sentences:
        list_id = []
        words = sentence.split()
        for i, word in enumerate(words):
            if i >= max_len:
                break
            if word in word2id.keys():
                list_id.append(word2id[word])
            else:
                list_id.append(0)
        list_id += [-1] * (max_len - len(list_id))
        data.append(list_id)
    return data
