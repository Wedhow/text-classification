import numpy as np
from collections import Counter
import chainer
from chainer import serializers
import train
import nets
import preproc
from sklearn.metrics import f1_score


def f_score(args):
    labels_true, sentences = train.load_data(args.test_data)
    embeddings, dic = preproc.load_embeddings(args.embeddings, args.n_vocab, args.n_units)
    sentences = preproc.convert2id(sentences, dic, args.max_len)
    n_label = len(Counter(labels_true))
    sentences_conv = chainer.dataset.concat_examples(sentences)
    model = nets.MLP(args.n_vocab, args.n_units, n_label, initialW=embeddings)
    serializers.load_npz(args.model, model)
    out = model(sentences_conv)
    predictions = np.argmax(out.array, axis=1)
    labels_pred = predictions.tolist()
    f_value = f1_score(labels_true, labels_pred, average='macro')
    print(f_value)
